# Game21, a.k.a. 'Game Thing'

Simulation experiments and suchlike.

## 2021 note

Dear visitor!
This is kind of a gnarly codebase that I was working on in 2016-2017.
It is self-consciously a single-developer project,
where that single developer was myself,
so I traded good documentation and stability in order to make as much progress as I could in the bits of free time that I had.

Let go of any expectation that this is a single cohesive project.
Think of it more as several experiments crammed into one codebase
(which I have since decided is a bad idea).
If you feel lost and wondering what this was trying to be: that's part of the ride.
Code is provided for entertainment purposes only.

That said, one self-contained game, 'Random Mazes', was spawned from this codebase.
You can read about it in [RANDO-README.md](./RANDO-README.md),
or [play it online](http://www.nuke24.net/plog/21.html).

Some of the things I wanted to do:
- 2.5D graphics which can be re-rendered to match the lighting context in which objects appear
- A networking model that treats the networking as part of the game;
  i.e. if you want to control an entity in the simulated (whether in your browser or on some server on the internet) world,
  you need a way to deliver IP packets to it.
  Parts of the network may be the real network, others may be simulated.
  It is not immediately obvious where the boundary is.
- Arbitrary world topologies via interconnected rooms which are internally cartesian
- A 2D, side-scrolling feel, but with 3D capabilities to allow occasional 'oh wow this is actually 3D' surprises
- Roller coasters.
  These would serve a similar purpose as Factorio's trains, but more physics-based and they go whoooooosh
  and that's fun and cool in a similar way to going fast in Sonic.

Some of those ideas got built out, some didn't.
Anything demo-ready was put on www.nuke24.net/plog/.

The following is the rest of the README in its original form.
Be aware that the build steps probably won't work
and new versions of Visual Studio Code will consider any .ts file with dependencies invalid
bbecause something about the way imports work in TypeScript changed since I wrote it.

## Building

```node build.js run-unit-tests``` to build and run the unit tests.

```node build.js js-libs``` to build both client (AMD) and server (CJS)-side libraries.

```make``` also works, but is basically an alias for ```node build.js```.

Demo pages are in ```demos/```.
For them to work you'll need PHP (to evaluate the php files) and to have built the client-side libraries.
Some demos may require some servers to be running or something.

I sometimes write down [ideas for how to do things](thoughts/).
Not all of these become reality.
Some of them end up on [TOGoS's project log](http://www.nuke24.net/plog/).

I'm going to try to keep the real conceptual stuff in thoughts/
and documentation on practices I'm actually following here in README.


## Coordinate system

Throughout this codebase I assume a right-handed coordinate system
where +X = right, +Y = down, and +Z = forward (into the screen).

Distance units are assumed to be 'meters' unless specified otherwise.
When dealing with raster data, the size of a pixel must be explicitly provided
(usually in terms of 'pixels per meter', which is what 'resolution' refers to)
to convert to physical distance units.



## Simulator/simulated separation

The world being simulated is represented as 'dumb' data objects.
Lots of interfaces with all data, no behavior.
Those interfaces should be designed with these principles in mind:

- Try to keep them as declarative as possible;
  Don't think about how it will be simulated;
- Parts of large object trees that are relatively static and have potential to be re-used
  should be referenced by URN instead of directly contained in parent objects.

This decouples the simulated world from the simulation implementation,
and gives a lot more flexibiliy to the simultion implementation.


## XML-style namespaces

Used to identify intrinsic concepts.

Probably should document allocated names somewhere.

General guidelines:

Names for top-level classes:

```http://ns.nuke24.net/Game21/FunThing```

Names for subclasses are of the format ```TopLevelClass/SubClass```:

```http://ns.nuke24.net/Game21/FunThing/SpecialFunThing```

Names for specific instances are of the format ```Collection/Item```:

```http://ns.nuke24.net/Game21/FunThings/TheBrownOne```

Note that the 'collection' namespace is plural, while the supertype is not.


## Router

There's a router program included for bridging real and simulated IP networks.

IPv6 is assumed, because nobody wants to deal with trying to allocate
IPv4 addresses for a bunch of simulated objects.

### Router deployment

- Check out Game21 project into some directory somewhere
- Edit .env to set network addresses that will be routed to your machine

As root:

- Install node, npm, make, tun2udp
- ```sysctl net.ipv6.conf.all.forwarding=1```
- Also edit ```/etc/sysctl.conf``` to change that setting permanently.
- Create a startup script (maybe just add to ```/etc/rc.local```:
  ```
  cd /path/to/Game21
  ./runtun >tun2udp.log 2>&1 &
  echo "$!" >tun2udp.pid
  ```
  - ```runtun``` will run tun2udp and set up routes as needed
- Also add a startup script to start the router in non-interactive mode, I guess.
  - Which doesn't yet exist.
